<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('admin/home', 'HomeController@adminHome')->name('admin.home')->middleware('is_admin');

Route::get('customer/register', 'Customer\RegisterController@register');
Route::post('customer/register', 'Customer\RegisterController@postRegister')->name('customer.register');


Route::get('vendor/register', 'Vendor\RegisterController@register');
Route::post('vendor/register', 'Vendor\RegisterController@postRegister')->name('vendor.register');

Route::group(['prefix' => 'customer', 'as' => 'customer.' , 'middleware' => 'IsCustomer'], function()
{
    // Route::get('register', 'Customer\RegisterController@register');
    // Route::post('register', 'Customer\RegisterController@postRegister')->name('register');
    Route::get('dashboard', 'Customer\DashboardController@home') ->name('dashboard');
});

Route::group(['prefix' => 'vendor', 'as' => 'vendor.', 'middleware' => 'IsVendor'], function()
{
    // // Route::get('register', 'Vendor\RegisterController@register');
    // Route::post('register', 'Vendor\RegisterController@postRegister')->name('register');
    Route::get('dashboard', 'Vendor\DashboardController@home') ->name('dashboard');
});

Route::group(['middleware' => 'CheckLogin'], function(){
    Route::get('/', function () {
        return view('auth/login');
    })->name('landing');
});