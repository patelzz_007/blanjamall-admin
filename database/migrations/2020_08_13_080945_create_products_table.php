<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('store_id')->unsigned();
            $table->foreign('store_id')->references('id')->on('stores');
            $table->string('product_name')->nullable();
            $table->decimal('retail_price', 15, 4)->default(0);
            $table->decimal('cost_price', 15, 4)->default(0);
            $table->decimal('discounted_price', 15, 4)->default(0);
            $table->decimal('rating', 5, 2)->default(0);
            $table->longText('product_description')->nullable();
            $table->string('product_tag')->nullable();
            $table->string('serial_number')->nullable();
            $table->string('warranty_period')->nullable();
            $table->string('order_by')->nullable();
            $table->integer('stock')->default(0);
            $table->integer('status')->default(0);
            $table->decimal('weight', 10, 5)->default(0);
            $table->integer('height')->default(0);
            $table->integer('width')->default(0);
            $table->integer('length')->default(0);
            
            $table->softDeletes();
            $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
