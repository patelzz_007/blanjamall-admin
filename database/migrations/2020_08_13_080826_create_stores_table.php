<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->id();

            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            
            $table->bigInteger('store_category_id')->nullable();
            $table->bigInteger('product_category_id')->nullable();
            

            $table->string('name')->nullable();
            $table->string('address1')->nullable();
            $table->string('address2')->nullable();
            $table->string('city')->nullable();
            $table->string('postcode')->nullable();
            $table->string('state')->nullable();
            $table->string('country')->nullable();
            $table->string('registration_number')->nullable();
            $table->string("company_name")->nullable();
            $table->string("contact_no")->nullable();
            $table->string('email_address')->nullable();
            $table->string('maps_url')->nullable();
            $table->text('description')->nullable();

            $table->string('cover_image')->nullable();
            
            $table->integer('status')->default('0');
            $table->string('account_number')->nullable();
            $table->string('bank')->nullable();
            $table->string('API_key_payment_gateway')->nullable();
           
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
