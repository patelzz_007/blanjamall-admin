<?php

namespace App\Http\Middleware;

use Closure;

class CheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!\Auth::user()){
            return redirect()->route('login');
        }else if (\Auth::user()->hasRole(['vendor'])){
            return redirect()->route('vendor.dashboard');
        }else if (\Auth::user()->hasRole(['customer'])){
            return redirect()->route('customer.dashboard');
        }else if (\Auth::user()->hasRole(['admin'])){
            return redirect()->route('admin.dashboard');
        }
        return redirect()->route('login');
    }
}
