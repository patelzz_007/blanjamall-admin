<?php

namespace App\Http\Middleware;

use Closure;

class IsCustomer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    
    public function handle($request, Closure $next)
    {
        if(!\Auth::user() || !\Auth::user()->hasRole(['customer'])){
            return redirect()->route('landing');
        }

        return $next($request);
    }
}
