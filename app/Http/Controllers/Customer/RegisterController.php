<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Store;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Route;

class RegisterController extends Controller
{
    public function register()
    {
        return view ('Customer.register');
    }

    public function postRegister(Request $request)
    {
        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        
        ]);


        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);

        $user->assignRole('customer');

        if(auth()->attempt(array('email' => $input['email'], 'password' => $input['password'])))
        {
            if (auth()->user()->hasRole(['vendor'])) {
                return redirect()->route('vendor.dashboard');
            }
            if (auth()->user()->hasRole(['customer'])) {
                return redirect()->route('customer.dashboard');
            }else{
                return redirect()->route('admin.dashboard');
            }
        }
        
    }
}
